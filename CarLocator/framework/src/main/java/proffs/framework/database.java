package proffs.framework;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by james on 10/01/17.
 */

public abstract class database extends ContentProvider {

    public String tableName;


    //-GLOBAL VARIBLES==============================================================================
    private static final String TAG_NAME = "DATABASE";
    //-Content Types -------------------------------------------------------------------------------
    static final String PROVIDER_NAME = "proffs.provider.database";
    private static String createTableSql;





    static final String PATH_MESSAGES = "messages";
    static final String PATH_USERS = "users";
    static final String PATH_TOPICS = "topics";
    public static final Uri URI_MESSAGES = Uri.parse("content://" + PROVIDER_NAME + "/" + PATH_MESSAGES);
    public static final Uri URI_USERS = Uri.parse("content://" + PROVIDER_NAME + "/" + PATH_USERS);
    public static final Uri URI_TOPICS = Uri.parse("content://" + PROVIDER_NAME + "/" + PATH_TOPICS);

    //-Content Types -------------------------------------------------------------------------------
    public static final String msg_id = "mid";
    public static final String msg_uid = "uID";
    public static final String msg_topic = "topic";
    public static final String msg_time_sent = "epoch_sent";
    public static final String msg_time_seen = "time_seen";
    public static final String msg_time_received = "time_received";
    public static final String msg_time_edited = "time_edited";
    public static final String msg_content = "content";

    public static final String users_id = "uid";
    public static final String users_username = "username";
    public static final String users_firstname = "firstname";
    public static final String users_lastname = "lastname";
    public static final String users_email = "email";

    public static final String topics_name = "name";
    public static final String topics_ids = "ids";

    //-Uri Matcher----------------------------------------------------------------------------------
    static final int MESSAGES = 1;
    static final int MESSAGES_ID = 2;
    static final int USERS = 3;
    static final int USERS_ID = 4;
    static final int TOPICS = 5;
    static final int TOPICS_ID = 6;

    static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static{
        uriMatcher.addURI(PROVIDER_NAME, PATH_MESSAGES, MESSAGES);
        uriMatcher.addURI(PROVIDER_NAME, PATH_MESSAGES + "/#", MESSAGES_ID);
        uriMatcher.addURI(PROVIDER_NAME, PATH_USERS, USERS);
        uriMatcher.addURI(PROVIDER_NAME, PATH_USERS + "/#", USERS_ID);
        uriMatcher.addURI(PROVIDER_NAME, PATH_TOPICS, TOPICS);
        uriMatcher.addURI(PROVIDER_NAME, PATH_TOPICS + "/#", TOPICS_ID);
    }

    //-Database specific constant declarations------------
    private SQLiteDatabase db;
    static final String DATABASE_NAME = "ComCenter";
    static final String MESSAGES_TABLE_NAME = "messages";
    static final String USERS_TABLE_NAME = "user_data";
    static final String TOPICS_TABLE_NAME = "topics_layout";
    static final int DATABASE_VERSION = 1;
    static final String CREATE_DB_TABLE_MESSAGES =
            " CREATE TABLE " + MESSAGES_TABLE_NAME + "(" +
                    msg_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    msg_uid + " INTEGER NOT NULL, "  +
                    msg_topic + " TEXT NOT NULL, "   +
                    msg_time_sent + " INTEGER, "    +
                    msg_time_received + " INTEGER, " +
                    msg_time_seen + " INTEGER, "     +
                    msg_time_edited + " INTEGER, "     +
                    msg_content + " TEXT NOT NULL);";
    static final String CREATE_DB_TABLE_USERS =
            "CREATE TABLE " + USERS_TABLE_NAME + " (" +
                    users_id + " INTEGER NOT NULL, "   +
                    users_username + " TEXT NOT NULL, " +
                    users_firstname + " TEXT, "         +
                    users_lastname + " TEXT, "          +
                    users_email + " TEXT);";
    static final String CREATE_DB_TABLE_TOPICS =
            "CREATE TABLE " + TOPICS_TABLE_NAME + " (" +
                    topics_ids + " TEXT NOT NULL, "   +
                    topics_name + " TEXT NOT NULL);";

    //-CONTRUCTOR ==================================================================================
    private ContentResolver contentResolver;
    private Context context;
    //this contructor is invoked for custom methods which call contentHandler methods
    public database(Context context){
        this.contentResolver = context.getContentResolver();
        //context.getContentResolver();
        this.context = context;
    }
    public database(){}

    //-HELP CLASS FOR MANAGEMENT-===================================================================
    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            db.execSQL(createTableSql);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d(TAG_NAME,"dropping and upgrading database");
            db.execSQL("DROP TABLE IF EXISTS " +  MESSAGES_TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " +  USERS_TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " +  TOPICS_TABLE_NAME);
            onCreate(db);
        }
    }


    @Override
    public boolean onCreate() {
        Context context = getContext();
        DatabaseHelper dbHelper = new DatabaseHelper(context);

        //Create database if it doesn't already exist.
        db = dbHelper.getWritableDatabase();
        return (db != null);
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowID;
        switch(uriMatcher.match(uri)){
            case MESSAGES:
            case MESSAGES_ID:
                rowID = db.insert(MESSAGES_TABLE_NAME, "", values);
                break;
            case USERS:
            case USERS_ID:
                rowID = db.insert(USERS_TABLE_NAME, "", values);
                break;
            case TOPICS:
            case TOPICS_ID:
                rowID = db.insert(TOPICS_TABLE_NAME, "", values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        //if insert successful
        if (rowID > 0){
            Uri _uri = ContentUris.withAppendedId(uri, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        throw new SQLException("Failed to add message into " + uri);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        switch(uriMatcher.match(uri)){
            case MESSAGES_ID:
                qb.appendWhere(msg_id + "=" + uri.getPathSegments().get(1));
            case MESSAGES:
            case USERS:
            case USERS_ID:
                qb.setTables(USERS_TABLE_NAME);
                break;
            case TOPICS:
            case TOPICS_ID:
                qb.setTables(TOPICS_TABLE_NAME);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        Cursor c = qb.query(db,	projection,	selection, selectionArgs,null, null, sortOrder);

        //register to watch a content URI for changes
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs){
        int count = 0;

        switch (uriMatcher.match(uri)){
            case MESSAGES:
                count = db.delete(MESSAGES_TABLE_NAME, selection, selectionArgs);
                break;
            case MESSAGES_ID:
                String id = uri.getPathSegments().get(1);
                count = db.delete( MESSAGES_TABLE_NAME, msg_id +  " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;
            case USERS:
                count = db.delete(USERS_TABLE_NAME, selection, selectionArgs);
                break;
            case TOPICS:
                count = db.delete(TOPICS_TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int count;

        switch (uriMatcher.match(uri)){
            case MESSAGES:
                count = db.update(MESSAGES_TABLE_NAME, values, selection, selectionArgs);
                break;
            case MESSAGES_ID:
                count = db.update(MESSAGES_TABLE_NAME, values, msg_id + " = " + uri.getPathSegments().get(1) +
                        (!TextUtils.isEmpty(selection) ? " AND (" +selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri );
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
