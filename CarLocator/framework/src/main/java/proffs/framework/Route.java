package proffs.framework;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by SERLIEM M ANAS on 08/01/2017.
 *
 * Class to model the variables needed to route between two point on the map.
 */


public class Route {

    /**
     * Distance between two given points
     */
    public String distance;
    /**
     * Duration to walk between two given points
     */
    public String duration;
        public String endAddress;
    /**
     * Start of the location latitude and Longitude
     */
    public LatLng startLocation;
    /**
     * End of the location latitude and longitude
     */
    public LatLng endLocation;
        public String startAddress;
        ;
        /**
         * List of latitude and longitudes.
         */
        public List<LatLng> coordinates;
}
