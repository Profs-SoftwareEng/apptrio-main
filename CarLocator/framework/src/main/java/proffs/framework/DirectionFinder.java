package proffs.framework;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SERLIEM M ANAS on 08/01/2017.
 *
 * A controller class for finding information of between the distance from
 * origin (Starting position) to destination (final destination). This class use Google map direction API
 * to find the path between two places. The output for the Google map direction output either json or xml,
 * but as of this class json is used as recommended by Google.
 *
 */

public class DirectionFinder {
    /**
     * Link to get the json(JavaScript Object Notation) file.
     */
    private static final String DIRECTION_URL_API = "https://maps.googleapis.com/maps/api/directions/json?";
    /**
     * GOOGLE API KEY
     */
    private static final String GOOGLE_API_KEY = "AIzaSyDcEAHGglMS9wLiyv2vX9Rpu5TJcke7Bus";
    private DirectionFinderListener listener;
    /**
     * Coordinates for current location
     */
    private LocationModel origin;
    /**
     * Coordinates for destination
     */
    private LocationModel destination;

    /**
     * DirectionFinder constructor
     * @param listener DirectionFinderListener
     * @param origin     Current location latitude and longitude
     * @param destination destination location latitude and longitude.
     */
    public DirectionFinder(DirectionFinderListener listener, LocationModel origin, LocationModel destination) {
        this.listener = listener;
        this.origin = origin;
        this.destination = destination;
    }

    /**
     * Start the download
     * @throws UnsupportedEncodingException
     */
    public void executeWalking() throws UnsupportedEncodingException {
        listener.onDirectionFinderStart();
        new DownloadRawData().execute(createWalkingUrl());
    }

    /**
     * Get, in String format, the walking route URL
     * @return the download URL in string format.
     * @throws UnsupportedEncodingException
     */
    private String createWalkingUrl() throws UnsupportedEncodingException {
        return DIRECTION_URL_API + "origin=" + origin.getLatitude() + "," + origin.getLongitude() + "&destination=" +
                destination.getLatitude() + "," + destination.getLongitude() +"&mode=walking&key=" + GOOGLE_API_KEY;
    }

    /**
     * Start the download
     * @throws UnsupportedEncodingException
     */
    public void executeDriving() throws UnsupportedEncodingException {
        listener.onDirectionFinderStart();
        new DownloadRawData().execute(createDrivingUrl());
    }

    /**
     * Get, in String format, the driving route URL
     * @return the download URL in string format.
     * @throws UnsupportedEncodingException
     */
    private String createDrivingUrl() throws UnsupportedEncodingException {
        return DIRECTION_URL_API + "origin=" + origin.getLatitude() + "," + origin.getLongitude() +
                "&destination=" + destination.getLatitude() + "," + destination.getLongitude() +
                "&mode=driving&key=" + GOOGLE_API_KEY;
    }

    /**
     *DownloadRawData class extends AsyncTask which enables proper and easy use of the UI thread.
     * This class download the json data which is perform background operations and publish
     * results on the UI thread without having to manipulate threads and/or handlers.
     */
    private class DownloadRawData extends AsyncTask<String, Void, String> {
        /**
         *
         * @param params
         * @return
         */
        @Override
        protected String doInBackground(String... params) {
            String link = params[0];
            try {
                URL url = new URL(link);
                InputStream is = url.openConnection().getInputStream();
                StringBuffer buffer = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                return buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         *
         * @param res
         */
        @Override
        protected void onPostExecute(String res) {
            try {
                parseJSon(res);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * @param data
     * @throws JSONException
     */
    private void parseJSon(String data) throws JSONException {
        if (data == null)
            return;

        List<Route> routes = new ArrayList<Route>();
        JSONObject jsonData = new JSONObject(data);
        JSONArray jsonRoutes = jsonData.getJSONArray("routes");
        for (int i = 0; i < jsonRoutes.length(); i++) {
            JSONObject jsonRoute = jsonRoutes.getJSONObject(i);
            Route route = new Route();

            JSONObject overview_polylineJson = jsonRoute.getJSONObject("overview_polyline");
            JSONArray jsonLegs = jsonRoute.getJSONArray("legs");
            JSONObject jsonLeg = jsonLegs.getJSONObject(0);
            JSONObject jsonDistance = jsonLeg.getJSONObject("distance");
            JSONObject jsonDuration = jsonLeg.getJSONObject("duration");
            JSONObject jsonEndLocation = jsonLeg.getJSONObject("end_location");
            JSONObject jsonStartLocation = jsonLeg.getJSONObject("start_location");

            route.distance = jsonDistance.getString("text");
            route.duration = jsonDuration.getString("text");
            route.endAddress = jsonLeg.getString("end_address");
            route.startAddress = jsonLeg.getString("start_address");
            route.startLocation = new LatLng(jsonStartLocation.getDouble("lat"), jsonStartLocation.getDouble("lng"));
            route.endLocation = new LatLng(jsonEndLocation.getDouble("lat"), jsonEndLocation.getDouble("lng"));
            route.coordinates = decodePolyLine(overview_polylineJson.getString("points"));

            routes.add(route);
        }
        listener.onDirectionFinderSuccess(routes);
    }

    /**
     *
     * @param poly
     * @return
     */
    private List<LatLng> decodePolyLine(final String poly) {
        int len = poly.length();
        int index = 0;
        List<LatLng> decoded = new ArrayList<LatLng>();
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int b;
            int shift = 0;
            int result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            decoded.add(new LatLng(
                    lat / 100000d, lng / 100000d
            ));
        }

        return decoded;
    }
}
