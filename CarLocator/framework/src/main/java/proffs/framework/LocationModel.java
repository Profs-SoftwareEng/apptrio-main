package proffs.framework;

import android.util.Log;

/**
 * Created by srd14wau on 25/10/2016.
 */

public class LocationModel {
    private double longitude;
    private double latitude;

    public LocationModel(double latitude, double longitude){
        if(latitude < 10.0){
            Log.d("LocationModel", "INCORRECT USAGE! : " + latitude + ", " + longitude);
        }
        this.longitude = longitude;
        this.latitude = latitude;
        Log.d("LOCATIONMODEL", this.latitude + "/ " + this.longitude);
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
