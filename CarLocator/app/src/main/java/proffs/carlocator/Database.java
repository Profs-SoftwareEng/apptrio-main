package proffs.carlocator;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DecimalFormat;

import proffs.framework.LocationModel;



public class Database extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "CarLocatorDB";
    private static final String TABLE_LOCATIONS = "Locations";
    private static final String TABLE_TIME = "Time";

    //locations
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";
    private static final String KEY_TIME = "time";

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //create database
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOCATIONS_TABLE = "CREATE TABLE " + TABLE_LOCATIONS
                + "(" + KEY_LATITUDE + " TEXT," + KEY_LONGITUDE + " TEXT," + KEY_TIME + " DATETIME DEFAULT CURRENT_TIMESTAMP)";
        db.execSQL(CREATE_LOCATIONS_TABLE);

        String CREATE_TIME_TABLE = "CREATE TABLE " + TABLE_TIME
                + "(" + KEY_TIME + " TEXT)";
        db.execSQL(CREATE_TIME_TABLE);
    }

    //if exist, drop the new table.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST " + TABLE_LOCATIONS);
        db.execSQL("DROP TABLE IF EXIST " + TABLE_TIME);
        onCreate(db);
    }

    public CarPoint getCarPoint(){
        SQLiteDatabase db = this.getReadableDatabase();
        double lat = 0, lng = 0;
        String time = null;

        Cursor cursor = db.query(TABLE_LOCATIONS, new String[] {KEY_LATITUDE, KEY_LONGITUDE}, "" , new String[] {},"", "", "");
        if (cursor != null && cursor.moveToFirst()){
            lat = Double.parseDouble(cursor.getString(0));
            lng = Double.parseDouble(cursor.getString(1));
        }

        cursor = db.query(TABLE_TIME, new String[] {KEY_TIME}, "" , new String[] {},"", "", "");
        if (cursor != null && cursor.moveToFirst()){
            time = cursor.getString(0);
        }

        if(time != null && lat != 0 && lng != 0){
            return new CarPoint(lat, lng, Long.parseLong(time));
        }

        return null;
    }

    public void addCarPoint(CarPoint carPoint){
        DecimalFormat d = new DecimalFormat("###.#############");
        String lat = d.format(carPoint.getLocationModel().getLatitude());
        String lng = d.format(carPoint.getLocationModel().getLongitude());

        //insert location
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_LATITUDE, lat);
        values.put(KEY_LONGITUDE, lng);
        //inserting row
        db.insert(TABLE_LOCATIONS, null, values);

        //insert
        values = new ContentValues();
        values.put(KEY_TIME, carPoint.getTime());
        //inserting row
        db.insert(TABLE_TIME, null, values);
        db.close(); //closes database connection
    }

    //-----------------------------------

    //DELETE RECORDS
    //delete all
    public void deleteAll(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_LOCATIONS, null, null);
        db.delete(TABLE_TIME, null, null);
        db.close();
    }
}