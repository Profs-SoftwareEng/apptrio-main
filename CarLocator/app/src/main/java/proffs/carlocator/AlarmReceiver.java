package proffs.carlocator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by james on 16/01/17.
 */

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("Alarm_Receiver", "Alarm receiver called!");

        boolean status = intent.getExtras().getBoolean("status");
        Log.d("AlarmReceiver", "EXTRA->"+status+"<-");
        Intent intentAlarmAudio = new Intent(context, AlarmAudio.class);
        intentAlarmAudio.putExtra("status", status);
        context.startService(intentAlarmAudio);
    }
}