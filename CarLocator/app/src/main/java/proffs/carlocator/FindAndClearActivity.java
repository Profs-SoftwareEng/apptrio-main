package proffs.carlocator;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class FindAndClearActivity extends AppCompatActivity {

    private final int MY_PERMISSION_REQUEST_LOCATION = 1;

    ImageView unSetLocationImg, findCarImg;
    TextView unSetLocation, findCar, timer;
    private Database database;
    CarPoint carPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_and_clear);

        database = new Database(this);
        carPoint = database.getCarPoint();

        unSetLocationImg = (ImageView) findViewById(R.id.clear_location_text_holder);
        unSetLocationImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.deleteAll();
                ((CarLocator)getApplicationContext()).setLocation("");
                Intent intent = new Intent(getBaseContext(), SetLocationActivity.class);
                startActivity(intent);
            }
        });
        unSetLocation = (TextView) findViewById(R.id.set_location_text_view);
        unSetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.deleteAll();
                Intent intent = new Intent(getBaseContext(), SetLocationActivity.class);
                startActivity(intent);
            }
        });

        findCarImg = (ImageView) findViewById(R.id.find_car_text_holder);
        findCarImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), MapViewActivity.class);
                startActivity(intent);
            }
        });
        findCar = (TextView) findViewById(R.id.find_car_text_view);
        findCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkLocationPermission()) {
                    requestPermissionLocation();
                } else {
                    Intent intent = new Intent(getBaseContext(), MapViewActivity.class);
                    startActivity(intent);
                }
            }
        });

        if(((CarLocator)getApplicationContext()).getLocation().equals("")) {
            ServerHandler serv = ServerHandler.getInstance(getApplicationContext(), this);
            serv.getClosestLocation(database.getCarPoint());
        }
        else {
            TextView location = (TextView)findViewById(R.id.location_var);
            location.setText(((CarLocator)getApplicationContext()).getLocation());
        }

        long alarmEpoch = carPoint.getTime();
        long currentEpoch = System.currentTimeMillis() / 1000;
        long timeEpoch = alarmEpoch - currentEpoch;

        timer = (TextView) findViewById(R.id.time_left_var);
        new CountDownTimer(timeEpoch * 1000, 1000) {
            public void onTick(long millisUntilFinished) {
                int secsUntilFinished = (int)Math.floor((millisUntilFinished / 1000));
                int mins = (int)Math.floor(secsUntilFinished / 60) ;
                int hours = (int)Math.floor(mins / 60) ;

                secsUntilFinished %= 60;
                mins %= 60;

                timer.setText("0" + hours + ":" + (mins < 10 ? "0" + mins : mins) + ":" +
                        (secsUntilFinished < 10 ? "0" + secsUntilFinished : secsUntilFinished));
            }

            public void onFinish() {
                timer.setText("done!");
            }
        }.start();
    }
    /**
     *
     * @return True if the permission is granted and false otherwise.
     */
    private boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            return true;
        return false;
    }

    /**
     * request location access permission
     */
    private void requestPermissionLocation() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, MY_PERMISSION_REQUEST_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case MY_PERMISSION_REQUEST_LOCATION:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Intent intent = new Intent(getBaseContext(), MapViewActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(), "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }
}

