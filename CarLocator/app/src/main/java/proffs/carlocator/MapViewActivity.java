package proffs.carlocator;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import proffs.framework.MapsFragment;

public class MapViewActivity extends AppCompatActivity {

    private ImageView imageView;
    TextView timer, location;
    Database database;

    @Override
    public void onBackPressed() {
        Intent intent;
        if(database.getCarPoint() == null)
            intent = new Intent(MapViewActivity.this, SetLocationActivity.class);
        else
            intent = new Intent(MapViewActivity.this, FindAndClearActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_view);

        database = new Database(this);
        //LocationModel target = database.getCarLocation();
        CarPoint target = database.getCarPoint();

        MapsFragment mapsFragment = new MapsFragment();
        mapsFragment.setDestination(target.getLocationModel());
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.map_view, mapsFragment).commit();


        long alarmEpoch = target.getTime();
        long currentEpoch = System.currentTimeMillis() / 1000;
        long timeEpoch = alarmEpoch - currentEpoch;

        timer = (TextView) findViewById(R.id.time_left_var);
        new CountDownTimer(timeEpoch * 1000, 1000) {
            public void onTick(long millisUntilFinished) {
                int secsUntilFinished = (int)Math.floor((millisUntilFinished / 1000));
                int mins = (int)Math.floor(secsUntilFinished / 60) ;
                int hours = (int)Math.floor(mins / 60) ;

                secsUntilFinished %= 60;
                mins %= 60;

                timer.setText("0" + hours + ":" + (mins < 10 ? "0" + mins : mins) + ":" +
                        (secsUntilFinished < 10 ? "0" + secsUntilFinished : secsUntilFinished));
            }

            public void onFinish() {
                timer.setText("done!");
            }
        }.start();

        location = (TextView)findViewById(R.id.location_var);
        location.setText(((CarLocator)getApplicationContext()).getLocation());
    }
}
