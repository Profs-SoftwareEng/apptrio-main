package proffs.carlocator;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import proffs.framework.LocationPointController;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.widget.Toast.LENGTH_LONG;

/**
 * Set the user's location and save the time set.
 */

public class SetLocationActivity extends AppCompatActivity{
    private static final  int MY_PERMISSION_REQUEST_LOCATION = 1;

    ImageView arrow;

    private LocationPointController locationPointController;
    private Database database;
    private PendingIntent pendingIntent;
    AlarmManager alarmManager;

    Spinner spinnerHour;
    Spinner spinnerMin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_location);

        database = new Database(this);

        //check if car point set
        if(database.getCarPoint() != null){
            Intent intent = new Intent(this, FindAndClearActivity.class);
            startActivity(intent);
        }

        alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);

        //save current location and time. Also, check if location is allowed, if yes, go ahead, and request otherwise.
        arrow = (ImageView) findViewById(R.id.enter_arrow);
        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkLocationPermission()) {
                    requestPermissionLocation();
                } else {
                    goToMapActivity();
                }
            }
        });

        spinnerHour = (Spinner) findViewById(R.id.spinnerHour);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapterHour = ArrayAdapter.createFromResource(this,
                R.array.hours_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapterHour.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerHour.setAdapter(adapterHour);

        spinnerMin = (Spinner) findViewById(R.id.spinnerMin);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapterMin = ArrayAdapter.createFromResource(this,
                R.array.mins_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapterMin.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerMin.setAdapter(adapterMin);
    }


    public void goToMapActivity(){
        //set timer
        int hour = Integer.parseInt(spinnerHour.getSelectedItem().toString());
        int  minute = Integer.parseInt(spinnerMin.getSelectedItem().toString());
        long currentEpoch = System.currentTimeMillis();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        String inputString = hour + ":" + (minute == 0 ? "00" : minute) + ":00";

        Date date = null;
        try {
            date = sdf.parse("1970-01-01 0" + inputString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("in milliseconds: " + date.getTime());
        long alarmEpoch = currentEpoch + date.getTime();

        Log.d("go to map", hour + " : " + minute + ":00");

        final Intent alarmIntent = new Intent(this, AlarmReceiver.class);

        alarmIntent.putExtra("status", true);

        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.set(AlarmManager.RTC_WAKEUP, alarmEpoch - 900000, pendingIntent);

        setLocation(alarmEpoch/1000);

        Intent intent = new Intent(getApplicationContext(), FindAndClearActivity.class);
        startActivity(intent);
    }

    /**
     * Set current location given time.
     * @param time
     */
    public void setLocation(long time){
        locationPointController = new LocationPointController(getApplicationContext());

        double lat = locationPointController.getPoints().getLatitude();
        double lng = locationPointController.getPoints().getLongitude();

        database.deleteAll();
        database.addCarPoint(new CarPoint(lat,lng, time));
        Toast.makeText(this, "Location Set!", LENGTH_LONG).show();
    }

    /**
     *
     * @return True if the permission is granted and false otherwise.
     */
    private boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            return true;
        return false;
    }

    /**
     * request location access permission
     */
    private void requestPermissionLocation() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, MY_PERMISSION_REQUEST_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case MY_PERMISSION_REQUEST_LOCATION:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    goToMapActivity();
                }else{
                    Toast.makeText(getApplicationContext(), "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }
}