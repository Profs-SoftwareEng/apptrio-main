package proffs.carlocator;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.TextView;

import proffs.framework.Server;

/**
 * Created by james on 17/01/17.
 */

public class ServerHandler extends Server {
    private static ServerHandler instance;

    String TAG = "SERVER";
    Context context;
    Activity activity;

    //set host
    private ServerHandler(Context context, Activity activity){
        super("www.X.4irc.com");
        this.context = context;
        this.activity = activity;
    }

    //singleton
    public static ServerHandler getInstance(Context context, Activity activity){
        if(instance == null){
            instance = new ServerHandler(context, activity);
        }
        return instance;
    }

    //ENDPOINT===========================================================
    public void getClosestLocation(CarPoint carPoint){
        String params = "lat=" + carPoint.getLocationModel().getLatitude()
                + "&lng=" + carPoint.getLocationModel().getLongitude();
        hitServer("app-trio/getClosestLocation.php", params);
        Log.d("SERVER", params);
    }

    @Override
    public void processResult(String endpoint, String result) {
        switch(endpoint) {
            case "app-trio/getClosestLocation.php":
                processGetClosestLocation(result);
                break;
        }
        Log.d(TAG, endpoint + ": " + result);
    }

    @Override
    public void processImageResult(Bitmap result) {
        //not used
    }

    public void processGetClosestLocation(String result){
        result = result.replace("\n", "");
        if(result.length() > 27){
            result = result.substring(0, 27) + "..";
        }
        TextView locationName = (TextView) activity.findViewById(R.id.location_var);
        locationName.setText(result);
        Log.d("processGetClosestLoc", "|"+result+"|");

        //update global
        ((CarLocator)context.getApplicationContext()).setLocation(result);

    }
}

