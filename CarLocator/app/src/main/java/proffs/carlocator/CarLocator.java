package proffs.carlocator;

import android.app.Application;

import proffs.framework.LocationPoint;

/**
 * Created by james on 18/01/17.
 */

public class CarLocator extends Application{
    String location = "";
    //LocationPoint locationPoint = null;
    CarPoint carPoint = null;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
