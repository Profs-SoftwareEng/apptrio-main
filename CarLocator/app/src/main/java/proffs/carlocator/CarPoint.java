package proffs.carlocator;

import proffs.framework.LocationModel;
import proffs.framework.LocationPoint;

/**
 * Created by james on 18/01/17.
 */

public class CarPoint extends LocationPoint {
    long time;

    CarPoint (double latitude, double longitude, long time){
        super.setLocationModel(new LocationModel(latitude,longitude));
        this.time = time;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
