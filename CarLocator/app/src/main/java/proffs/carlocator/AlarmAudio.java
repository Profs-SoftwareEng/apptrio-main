package proffs.carlocator;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by james on 16/01/17.
 */

public class AlarmAudio extends Service {
    MediaPlayer mediaPlayer;
    int startID;
    boolean isRunning = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("AlarmAudio", "onStartCommand");

        boolean status = intent.getExtras().getBoolean("status");
        Log.d("AlarmAudio", "EXTRA->"+status+"<-");
        if(status){
            mediaPlayer = MediaPlayer.create(this, R.raw.alarm);
            if(!isRunning){
                mediaPlayer.start();
                isRunning = true;

                Intent i = new Intent();
                i.setAction(Intent.ACTION_MAIN);
                i.addCategory(Intent.CATEGORY_LAUNCHER);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ComponentName cn = new ComponentName(this, AlarmActivity.class);
                i.setComponent(cn);
                startActivity(i);
            }
        }
        else if(isRunning){
            mediaPlayer.stop();
            mediaPlayer.reset();
            isRunning = false;
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        Toast.makeText(this, "Alarm Audio Server Destroyed", Toast.LENGTH_SHORT).show();
    }
}

