package proffs.framework;

/**
 * Created by srd14wau on 25/10/2016.
 */

public abstract class FrameworkCopyright {
    private String CopyrightText = "This application is based on the Simple Android Application Framework. (c) University\n" +
        "of East Anglia 2016.";


    public final String getCopyright() {
        return CopyrightText + "\n\n" + getAppCopyright();
    }

    protected abstract String getAppCopyright();
}
