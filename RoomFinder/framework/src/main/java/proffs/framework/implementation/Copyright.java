package proffs.framework.implementation;

import proffs.framework.FrameworkCopyright;

/**
 * Created by james on 19/01/17.
 */

public class Copyright extends FrameworkCopyright {
    @Override
    protected String getAppCopyright() {
        return "Made by (c) The Professionals 2017 ";
    }
}
