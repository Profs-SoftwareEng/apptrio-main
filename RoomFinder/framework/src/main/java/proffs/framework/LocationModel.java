package proffs.framework;

/**
 * Created by srd14wau on 25/10/2016.
 */

public class LocationModel {
    private double longitude;
    private double latitude;

    public LocationModel(){}

    public LocationModel(double latitude, double longitude){
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "Latitude: " + latitude + "  Longitude: " + longitude;
    }
}
