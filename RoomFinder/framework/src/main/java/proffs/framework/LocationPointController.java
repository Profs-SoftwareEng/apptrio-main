package proffs.framework;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import proffs.framework.LocationModel;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.widget.Toast.LENGTH_LONG;

/**
 * Created by SERLIEM M ANAS on 10/01/2017.
 */

public class LocationPointController implements LocationListener {

    private LocationModel origin;

    private Context context;

    private LocationManager locationManager;

    private Location location;

    public LocationModel getPoints() {
        return this.origin;
    }

    public LocationPointController(Context context){
        this.context = context;
        getCurrentLocation();
    }

    public float getDistance(LocationModel locationModel){
        Log.d("CURRENT LOCATION!", origin.getLatitude() + " :  " + origin.getLongitude());
        Log.d("TARGET LOCATION!", locationModel.toString());

        Location currentLocation = new Location(" ");
        currentLocation.setLatitude(origin.getLatitude());
        currentLocation.setLongitude(origin.getLongitude());

        Location destination = new Location(" ");
        destination.setLatitude(locationModel.getLatitude());
        destination.setLongitude(locationModel.getLongitude());

        return currentLocation.distanceTo(destination);
    }

// 52.612612612612615 :  1.2463036457901757
// 52.612612612612615 :  1.2463036457901757

    public void getCurrentLocation() {
        try {
            if (ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                showSettingsAlert();
                Toast.makeText(context, "Permission Not Granted!!!!", LENGTH_LONG).show();
            }
            if (ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 50, 0, this);
                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                origin = new LocationModel(location.getLatitude(), location.getLongitude());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Not found!", LENGTH_LONG).show();
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("GPS Not Enabled");
        alertDialog.setMessage("Do you wants to turn On GPS");


        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }
    private static final String TAG = "TreasureHunt";
    @Override
    public void onLocationChanged(Location location) {

        origin.setLatitude(location.getLatitude());
        origin.setLongitude(location.getLongitude());

//        Toast.makeText(context, "lat: "+ origin.getLatitude()+"\n lng: "+origin.getLongitude(),Toast.LENGTH_SHORT ).show();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {    }

    @Override
    public void onProviderEnabled(String s) {    }

    @Override
    public void onProviderDisabled(String s) {    }
}