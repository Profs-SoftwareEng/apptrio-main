package proffs.framework;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Created by james on 11/11/16.
 * This class performs an asynchronous operation to hit the server and update the local database
 * with the response
 */

public abstract class Server {

    private String hostname;

    public Server(String hostname) {
        this.hostname = hostname;
    }

    public Bitmap getBitmapFromURL(String imageUrl) {
        try {
            java.net.URL url = new java.net.URL("http://" + hostname + "/" + imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String request(String endpoint, String params) {
        InputStream is = null;
        Log.d("SERVER", "hitting -> " + endpoint);

        int len = 15;
        try {
            //setup
            URL url = new URL("http://" + hostname + "/" + endpoint);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);

            //append POST data
            conn.setRequestProperty("Content-length", String.valueOf(params.length()));
            DataOutputStream output = new DataOutputStream(conn.getOutputStream());
            output.writeBytes(params);

            //Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            if (response != 200) {
                return Integer.toString(response);
            }

            Log.d("NetworkApp", "The response is: " + response);
            is = conn.getInputStream();
            // Convert the InputStream into a string and return
            return readIt(is, len);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return "404";
    }

    private String readIt(InputStream stream, int len) throws IOException, UnknownHostException {
        BufferedReader r = new BufferedReader(new InputStreamReader(stream));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line).append('\n');
        }
        return total.toString();
    }

    //---------------

    public void hitServer(String endpoint, String params){
        new serverRequest().execute(endpoint, params);
    }

    public void imageHitServer(String endpoint){
        new serverImageRequest().execute("app-trio/" + endpoint);
    }

    //==============================================================================================
    private class serverRequest extends AsyncTask<String, Void, String> {
        private String endpoint = "";

        @Override
        protected String doInBackground(String... urls) {
            endpoint = urls[0];
            Log.d("HIT SERVER", urls[0] + "?" + urls[1]);
            return request(urls[0], urls[1]);
        }

        @Override
        protected void onPostExecute(String result) {
            if (result == null) {
                return;
            }
            processResult(endpoint, result);
        }
    }

    private class serverImageRequest extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... urls) {
            return getBitmapFromURL(urls[0]);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result == null) {
                return;
            }
            processImageResult(result);
        }
    }

    public abstract void processResult(String endpoint, String result);
    public abstract void processImageResult(Bitmap result);
}
