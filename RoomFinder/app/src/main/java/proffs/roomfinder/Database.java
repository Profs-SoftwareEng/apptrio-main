package proffs.roomfinder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteOpenHelper {
    //Database version
    private static final int DATABASE_VERSION = 1;
    // database name
    private static final String DATABASE_NAME = "roomFinderDB";
    // history table name
    private static final String TABLE_HISTORY = "history";
    // history table collumns
    private static final String KEY_ROOM = "roomName";

    public Database(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db){
        String CREATE_HISTORY_TABLE = "CREATE TABLE " + TABLE_HISTORY + "(" + KEY_ROOM + " TEXT" +")";
        db.execSQL(CREATE_HISTORY_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        //drop older table if it exists
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);
        //creating tables again
        onCreate(db);
    }
    // INSERT RECORD
    //adding new room
    public void addRoom(RoomPoint room){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ROOM, room.getName());
        //inserting row
        db.insert(TABLE_HISTORY, null, values);
        db.close(); //closes database connection
    }
    // READ RECORDS
    //get one room
    public RoomPoint getRoom(String name){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_HISTORY, new String[] {KEY_ROOM}, KEY_ROOM + " = ?", new String[] {name},"", "", "");
        if (cursor != null && cursor.moveToFirst()){
            RoomPoint room = new RoomPoint(cursor.getString(0),0,0,"");
            return room;
        }
        return null;
    }
    // get all rooms
    public List<RoomPoint> getAllRooms(){
        List<RoomPoint> roomList = new ArrayList<RoomPoint>();
        // select all query
        String selectQuery = "SELECT * FROM " + TABLE_HISTORY;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        //looping through all rows and adding to list
        if (cursor.moveToFirst()){
            do {
                RoomPoint room = new RoomPoint("", 0,0,"");
                room.setName(cursor.getString(0));
                roomList.add(room);
            } while (cursor.moveToNext());
        }
        return roomList;
    }
    // get number of rooms in the database
    public int getRoomCount(){
        String countQuery = "SELECT * FROM " + TABLE_HISTORY;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        return cursor.getCount();
    }
    // UPDATING RECORDS
    //updating a room entry
    public int updateRoom(RoomPoint room){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ROOM, room.getName());
        //updating row
        return db.update(TABLE_HISTORY, values, KEY_ROOM + " = ?", new String[]{room.getName()});
    }
    // DELETING RECORDS
    // deleting a room entry from the history list
    public void deleteRoom(RoomPoint room){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_HISTORY, KEY_ROOM + " = ?", new String[]{room.getName()});
        db.close();
    }
    // delete all records
    public void deleteAll(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_HISTORY, null, null);
        db.close();
    }
};