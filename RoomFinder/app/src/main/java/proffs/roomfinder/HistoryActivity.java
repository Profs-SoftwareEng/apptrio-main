package proffs.roomfinder;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.*;
import java.util.*;
import android.view.*;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;


public class HistoryActivity extends AppCompatActivity {

    private static final int MY_PERMISSION_REQUEST_LOCATION = 1;

    ImageView backButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        //create list view
        final ListView listView = (ListView) this.findViewById(R.id.historyList);
        // create arraylist to hold list content
        final ArrayList<String> listContent = new ArrayList();
        //create example list for testing

        final Database db = new Database(this);
        final List<RoomPoint> historyList = db.getAllRooms();
        for (RoomPoint r: historyList){
            listContent.add(r.getName());
        }
        for (RoomPoint r: historyList){
            Log.d("Item: ", r.getName());
        }
        //DATABASE TESTING END=====================================================
        //create array adapter
        final StableArrayAdapter itemsAdapter = new StableArrayAdapter(this, android.R.layout.simple_list_item_1, listContent);
        // set the adapter to the listView
        listView.setAdapter(itemsAdapter);
        itemsAdapter.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                if (!checkLocationPermission()) {
                    requestPermissionLocation();
                } else {
                    final String roomName = (String) parent.getItemAtPosition(position);
                    Intent intent = new Intent(getBaseContext(), MapActivity.class);
                    Bundle b = new Bundle();
                    b.putString("st", roomName);
                    intent.putExtras(b);
                    startActivity(intent);
                    finish();
                }
            }
        });

        //Button navigation code
        backButton = (ImageView) findViewById(R.id.back_button_history);

        backButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent loadNewActivity = new Intent(HistoryActivity.this, HomeActivity.class);
                startActivity(loadNewActivity);
                //Log.d("history", "clicked");
                //itemsAdapter.add("NEW!");
            }
        });
        ImageView clearButton;
        clearButton = (ImageView) findViewById(R.id.clear_list_button);

        clearButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                listContent.removeAll(listContent);
                itemsAdapter.notifyDataSetChanged();
                db.deleteAll();
            }
        });
    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        ArrayList<String> list = new ArrayList<>();
        Context context;

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  ArrayList<String> objects) {
            super(context, textViewResourceId, objects);
            list = objects;
            this.context = context;
            for(String s : list){
                Log.d("asapter", s);
            }
        }

        @Override
        public long getItemId(int position) {
            //String item = getItem(position);
            //return tiolist.get(posin);
            //later
            return 0L;
        }

        @Override
        public void add(String object) {
            //super.add(object);
            Log.d("adapter", "ADD!");
            list.add(object);
        }


        @Override
        public boolean hasStableIds() {
            return true;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //return super.getView(position, convertView, parent);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.history_list_layout, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.content);
            textView.setText(list.get(position));
            return rowView;
        }
    }
    private boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            return true;
        return false;
    }

    private void requestPermissionLocation() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, MY_PERMISSION_REQUEST_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case MY_PERMISSION_REQUEST_LOCATION:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(getApplicationContext(), "Permission Granted!", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }
}
