package proffs.roomfinder;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import proffs.framework.Server;

/**
 * Created by james on 05/01/17.
 */

public class serverHandler extends Server {
    String TAG = "SERVER";
    Context context;
    Activity activity;

    //set host
    public serverHandler(Context context, Activity activity){
        super("www.X.4irc.com");
        this.context = context;
        this.activity = activity;
    }

    @Override
    public void processResult(String endpoint, String result) {
        result = result.replace("\n", "");
        if(result.replace(" ", "").equals("invalid")){
            Log.d("processGetRoom", "INVALID!");
            ((MapActivity)activity).setRoomPoint(null);
            return;
        }
        else {
            Log.d("processGetRoom", "Not invalid: |" + result + "|");
        }
        switch(endpoint){
            case "app-trio/getRooms.php":
                if(ifSingleRoom(result)){
                    processGetRoom(result);
                }
                else {
                    processGetRooms(result);
                }
                break;
        }
        Log.d(TAG, endpoint + ": " + result);
    }

    @Override
    public void processImageResult(Bitmap result) {
        //not used
    }

    private boolean ifSingleRoom(String result){
        try {
            JSONArray jsonArray = new JSONArray(result);
            if(jsonArray.length() == 1){
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    //ENDPOINT===========================================================

    public void getTest() {
        String params = "a=3";
        hitServer("app-trio/test.php", params);
        Log.d("SERVER", params);
    }

    public void getRooms(String searchTerm){
        String params = "st=" + searchTerm;
        hitServer("app-trio/getRooms.php", params);
        Log.d("SERVER", params);
    }

    //process
    public void processGetRoom(String result){
        try {
            JSONArray jsonArray = new JSONArray(result);
            JSONObject roomData = new JSONObject(jsonArray.getString(0));

            RoomPoint roomPoint = new RoomPoint(
                roomData.getString("name"),
                roomData.getDouble("lat"),
                roomData.getDouble("long"),
                roomData.getString("description")
            );
            ((MapActivity)activity).setRoomPoint(roomPoint);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("processGetRoom", result);
    }

    public void processGetRooms(String result){
        ArrayList<String> names = new ArrayList<>();

        try {
            JSONArray jsonArray = new JSONArray(result);
            for(int i = 0; i < jsonArray.length(); i++) {
                JSONObject roomData = new JSONObject(jsonArray.getString(i));
                names.add(roomData.getString("name"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ((HomeActivity)activity).adapter.clear();
        ((HomeActivity)activity).adapter.addAll(names);
        ((HomeActivity)activity).adapter.notifyDataSetChanged();
        Log.d("processGetRoom", result);
    }
}
