package proffs.roomfinder;

import proffs.framework.LocationModel;
import proffs.framework.MapsActivity;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

public class MapActivity extends AppCompatActivity {

    public RoomPoint roomPoint;

    @Override
    public void onBackPressed() {
        Log.d("back ", "fired!");
        Intent loadNewActivity = new Intent(MapActivity.this, HomeActivity.class);
        startActivity(loadNewActivity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);


        Bundle b = getIntent().getExtras();
        String searchTerm = ""; // or other values
        if(b != null)
            searchTerm = b.getString("st");
        Log.d("MapActivity", searchTerm);

        serverHandler serv = new serverHandler(getApplicationContext(), this);
        serv.getRooms(searchTerm);
    }

    public void setRoomPoint(RoomPoint roomPoint){
        if(roomPoint == null){
            Intent intent = new Intent(this, HomeActivity.class);
            Toast.makeText(this,"Invalid room, please select from list", Toast.LENGTH_LONG).show();
            startActivity(intent);
            return;
        }
        this.roomPoint = roomPoint;

        initMapFragment(roomPoint.getLocationModel());
        Log.d("MapActivity", roomPoint.getDescription());

        //add to history
        Database database = new Database(getBaseContext());
        if(database.getRoom(roomPoint.getName()) != null) {
            Log.d("MapActivity", "deleted!");
            database.deleteRoom(roomPoint);
        }
        else {
            Log.d("MapActivity", "not deleted!");
        }
        database.addRoom(roomPoint);
    }

    /**
     * Get the location of the room and  locate the room from current location
     */
    public void initMapFragment(LocationModel locationModel){

        MapsActivity mapsActivity = new MapsActivity();
        mapsActivity.setDestination(locationModel);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.map_view, mapsActivity).commit();

    }
}

