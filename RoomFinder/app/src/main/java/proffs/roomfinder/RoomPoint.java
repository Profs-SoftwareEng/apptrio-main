package proffs.roomfinder;

import proffs.framework.LocationModel;
import proffs.framework.LocationPoint;

/**
 * Created by Adam on 10/01/2017.
 */

public class RoomPoint extends LocationPoint {
    //global vars
    String description;
    //constructor
    RoomPoint (String name, double latitude, double longitude, String roomDescription){
        super.setLocationModel(new LocationModel(latitude,longitude));
        super.setName(name);
        this.description = roomDescription;
    }
    // getters and setters
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setLocationModel(LocationModel locationModel){
        super.setLocationModel(locationModel);
    }

    public LocationModel getLocationModel(){
        return super.getLocationModel();
    }

    public void setName(String name){
        super.setName(name);
    }

    public String getName(){
        return super.getName();
    }
}
