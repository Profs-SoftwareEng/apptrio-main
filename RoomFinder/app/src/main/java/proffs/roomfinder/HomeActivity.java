package proffs.roomfinder;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class HomeActivity extends AppCompatActivity {

    private static final int MY_PERMISSION_REQUEST_LOCATION = 1;

    ImageView visitedButton;

    AutoCompleteTextView autocomplete;
    public ArrayAdapter<String> adapter;
    public ArrayList<String> list;

    String searchTerm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        visitedButton = (ImageView) findViewById(R.id.visited_button);
        visitedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, HistoryActivity.class);
                startActivity(intent);
            }
        });

        String[] arr = {"Loading"};
        list = new ArrayList<>();
        for (String s : arr) {
            list.add(s);
        }

        autocomplete = (AutoCompleteTextView) findViewById(R.id.searchRoom);
        ArrayAdapter<String> autoadapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, list) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextSize(14);
                ((TextView) v).setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                return v;
            }
        };

        autocomplete.setThreshold(1);
        autocomplete.setAdapter(autoadapter);
        serverHandler serv = new serverHandler(getApplicationContext(), this);
        serv.getRooms("");

        adapter = autoadapter;

        autocomplete.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press

                    searchTerm = autocomplete.getText().toString();
                    if (!checkLocationPermission()) {
                        requestPermissionLocation();
                    } else {
                        Intent intent = new Intent(HomeActivity.this, MapActivity.class);
                        Bundle b = new Bundle();
                        b.putString("st", searchTerm);
                        intent.putExtras(b);
                        startActivity(intent);
                        finish();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            return true;
        return false;
    }

    private void requestPermissionLocation() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, MY_PERMISSION_REQUEST_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case MY_PERMISSION_REQUEST_LOCATION:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Bundle b = new Bundle();
                    b.putString("st", searchTerm);
                    Intent intent = new Intent(HomeActivity.this, MapActivity.class);
                    intent.putExtras(b);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }
}