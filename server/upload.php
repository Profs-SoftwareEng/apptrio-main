<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$dbconn = pg_connect("dbname=apptrio user=James password=planner555") 
			 or die('Could not connect to local database ' . pg_last_error());

echo "<div id='outer'>";
echo "<img style='width: 50px' src='https://image.freepik.com/free-icon/coins-stacks-of-dollars-hand-drawn-commercial-symbol_318-51955.jpg'> THE PROFESIONAL's <br>
Treasure hunt, treasure	point uploader<br><br>";

$target_dir = "th_images/";
if(isset($_FILES["fileToUpload"]["name"])){
	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
}

$output = "";

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
	//check all have and entry
		
	if(
		!empty($_POST["name"]) 
		&& !empty($_POST["description"]) 
		&& !empty($_POST["value"])
		&& !empty($_POST["longitude"])
		&& !empty($_POST["latitude"])){

		$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
		if($check !== false) {
		$output .= "File is an image - " . $check["mime"] . ".<br>";
		$uploadOk = 1;
		} else {
		$output .= "File is not an image.<br>";
		$uploadOk = 0;
		}

		// Check if file already exists
		if (file_exists($target_file)) {
		    $output .= "Sorry, file already exists.<br>";
		    $uploadOk = 0;
		}
		// Check file size
		if ($_FILES["fileToUpload"]["size"] > 5000000) {
		    $output .= "Sorry, your file is too large.<br>";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
		    $output .= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.<br>";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    $output .= "Sorry, your file was not uploaded.<br>";
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
			$output .= "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
		    } else {
			$output .= "Sorry, there was an error uploading your file.";
		    }
		}

		//db upload
echo "|".$target_file."|";
		
		$query = "INSERT INTO treasurepoint (name, description, image_path, value, longitude, latitude) VALUES ($1, $2, $3, $4, $5, $6)";
		$result = pg_query_params($query, array($_POST["name"], $_POST["description"], $target_file, $_POST["value"], $_POST["longitude"], $_POST["latitude"])) 
			or die ('Query fail: '.pg_last_error());
		$line = pg_fetch_array($result, null, PGSQL_ASSOC);
	}
	else {
		$output .= "Whole form needs to be filled out!<br>";
	}
}
?>

<!DOCTYPE html>
<style>
	input{
		float: right;
		border-radius: 5px;
		border: #d1d1d1 solid grey;
		width: 250px;
	}
	#fileToUpload{
		background: #d1d1d1;
		
	}
	#inner {
		width: 400px;
	 	height: 300px;
	 	
	}
	#outer{
		/*position: fixed; /* or absolute 
		top: 50%;
		left: 50%;
		
		transform: translate(-50%, -50%);*/
	}
	#treasurePoint{
		display: inline-block;
		float: left;
		margin-top: 30px;
	}
	#treasurePointImg{
		height:100px;
		display: inline-block;
		float: right;
		border-radius: 30px;
		padding: 0px;
		margin: 0px;
		position: relative;
		top: -45px;
	}
</style>
<html>
<body>
<div id="inner" >
	<form action="upload.php" method="post" enctype="multipart/form-data">
	    <strong>Name:</strong> <input type="text" name="name" id="name"><br>
	    <div style="height:5px;"></div>
	    <strong>Description:</strong> <input style="postition:relative; top: -50px;" type="text" name="description" id="description"><br>
	    <div style="height:5px;"></div>	    
  	    <strong>Base Value:</strong> <input  type="text" name="value" id="value"><br>
	    <div style="height:5px;"></div>	
	    <strong>Image:</strong> <input  type="file" name="fileToUpload" id="fileToUpload"><br>
	    <div style="height:5px;"></div>
	    <strong>Latitude:</strong> <input  type="text" name="latitude" id="latitude"><br>
	    <div style="height:5px;"></div>	    
	    <strong>Longitude:</strong> <input  type="text" name="longitude" id="longitude"><br>
	    <div style="height:5px;"></div>
	    <input type="submit" value="Upload Treasure Point" name="submit">
	</form>



<?php
	if(isset($output) && !empty($output)){
		echo "<br><br>------------------------------<br><p style='color: blue'>";
		echo $output."<p>";
	}

	//print current treasure points
	echo "<br><br>------------------------------<br>";
	$query = "SELECT * FROM treasurepoint ORDER BY id";
	$result = pg_query($query) 
		or die ('Query fail: '.pg_last_error());
	while($line = pg_fetch_array($result, null, PGSQL_ASSOC)){
		echo "<div id='treasurePoint'> <strong>Name:</strong> ".$line["name"].
		"<br><strong>Description: </strong>".$line["description"].
		"<br><strong>Value:</strong> ".$line["value"].
		"<br><strong>Latitude: </strong>".$line["latitude"].
		"<br><strong>Longitude: </strong>".$line["longitude"].
		"</div><img id='treasurePointImg' src='".$line["image_path"]."'><br><br>";
	}
?>
</div>
</div>
</body>
</html>
