<?php
    $lat = filter_input(INPUT_POST, 'lat');
    $lng = filter_input(INPUT_POST, 'lng');
//    $lat = 52.6456787;
//    $lng = 1.2456879;
    
    $currentVal = 1000000;
    $currentName = "";
    
    $dbconn = pg_connect("dbname=apptrio user=James password=planner555") 
			 or die('Could not connect to local database ' . pg_last_error());

    //query
    $out = array();
    $query = "SELECT lat, long, name FROM uea_map_data";
    $result = pg_query($query) or die ('Query fail: '.pg_last_error());
    while($line = pg_fetch_array($result, null, PGSQL_ASSOC)){
        $tempLatDiff = abs((double)$line['lat'] - $lat);
        $tempLngDiff = abs((double)$line['long'] - $lng);
        
        $temp = str_replace(" ", "", $line['name']);
        if((!empty($temp)) && $currentVal > ($tempLatDiff + $tempLngDiff)){
            $currentVal = ($tempLatDiff + $tempLngDiff);
            $currentName = $line['name'];
        }
    }
    echo $currentName;
?>
 