package proffs.framework;

import java.util.List;

/**
 * Created by SERLIEM M ANAS on 08/01/2017.
 * Interface class to implement the
 *
 */

public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);
}
