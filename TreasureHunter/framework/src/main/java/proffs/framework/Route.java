package proffs.framework;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by SERLIEM M ANAS on 08/01/2017.
 *
 * Class to model the variables needed to route between two point on the map.
 */


public class Route {

    /**
     * Distance between two given points
     */
    private String distance;
    /**
     * Duration to walk between two given points
     */
    private String duration;
    /**
     * Start of the location latitude and Longitude
     */
    private LatLng startLocation;
    /**
     * End of the location latitude and longitude
     */
    private LatLng endLocation;
        ;
        /**
         * List of latitude and longitudes.
         */
        private List<LatLng> coordinates;

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public LatLng getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(LatLng startLocation) {
        this.startLocation = startLocation;
    }

    public LatLng getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(LatLng endLocation) {
        this.endLocation = endLocation;
    }


    public List<LatLng> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<LatLng> coordinates) {
        this.coordinates = coordinates;
    }
}
