package proffs.framework;

import android.Manifest;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import proffs.framework.DirectionFinder;
import proffs.framework.DirectionFinderListener;
import proffs.framework.LocationModel;
import proffs.framework.R;
import proffs.framework.Route;

/**
 * MapssActivity class is a fragment for the map. It visualise the map, current location, destination and marked directions
 * on the map fragment.
 */

public class MapsFragment extends Fragment implements DirectionFinderListener, OnMapReadyCallback {

    private GoogleMap mMap;
    private static final int MY_PERMISSION_REQUEST_LOCATION = 1;

    /**
     * Current Location Markers
     */
    private List<Marker> originMarkers;
    /**
     * Destination location markers
     */
    private List<Marker> destinationMarkers;
    /**
     * the poly path line paths
     */
    private List<Polyline> polylinePaths;
    /**
     * progress dialogue
     */
    private ProgressDialog progressDialog;

    /**
     * current Location
     */
    public LocationModel origin;
    /**
     * user's specified destination
     */
    public LocationModel destination;

    /**
     * Fragment view
     */
    View myView;


    /**
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return The FrameLayout view
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        myView = inflater.inflate(R.layout.activity_maps, container, false);

        return myView;
    }

    /**
     *
     * @param view   The map fragment
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MapFragment mapFragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //get current Location
        getCurrentLocation();
        //find direction
        findDirection(origin, getDestination());
    }

    public void getCurrentLocation() {

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_REQUEST_LOCATION);
        }else {
            mMap.setMyLocationEnabled(true);
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            try{
                origin = new LocationModel(location.getLatitude(), location.getLongitude());
                LatLng latLng = new LatLng(origin.getLatitude(), origin.getLongitude());
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     *  gets direction from user's location to final destination.
     * @param origin  user's current location
     * @param destination user's target location
     */
    private void findDirection(LocationModel origin, LocationModel destination) {
        try {
            new DirectionFinder(this, origin, destination).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete the Direction existed on the Map
     */
    @Override
    public void onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(getActivity(), "","Loading..!", true);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }
        }
    }

    /**
     *  Show the directions on the map.
     * @param routes List of the routes from origin to destination.
     */
    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {

        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        for (Route route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.getStartLocation(), 18));

            //toast time and distance to reach destination
            Toast.makeText(getActivity(), route.getDuration() + " , "+ route.getDistance() , Toast.LENGTH_LONG).show();

            //mark destination
            destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .position(route.getEndLocation())));

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLUE).
                    width(15);

            for (int i = 0; i < route.getCoordinates().size(); i++)
                polylineOptions.add(route.getCoordinates().get(i));
            polylinePaths.add(mMap.addPolyline(polylineOptions));
        }
    }

    /**
     *
     * @return destination LocationModel
     */
    public LocationModel getDestination() {
        return destination;
    }

    public void setDestination(LocationModel destination) {
        this.destination = destination;
    }
}
