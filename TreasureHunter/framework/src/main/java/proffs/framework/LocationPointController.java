package proffs.framework;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import proffs.framework.LocationModel;

/**
 * Created by SERLIEM M ANAS.
 * This class Model the Location point controller to LocationModel which is also in Framework App. Also, it get the
 * user's current location continuously as user changes location.
 *
 */

public class LocationPointController implements LocationListener {

    /**
     * LocationModel to model for the current location
     */
    private LocationModel origin;

    private Context context;

    private LocationManager locationManager;

    private Location location;

    /**
     * @return The origin location in LocationModel Obeject
     */
    public LocationModel getPoints() {
        return this.origin;
    }

    /**
     * Constructor for the LocationPointController class. In the constructor, it also gets the current location of the user.
     * @param context  Activity Context
     */
    public LocationPointController(Context context) {
        this.context = context;
        getCurrentLocation();
    }

    /**
     * Calculate how close the user is to the  given locationModel
     * @param locationModel destination
     * @return the distance in meters between user's and given given destination, locationalModel.
     */
    public float getDistance(LocationModel locationModel) {
        Log.d("CURRENT LOCATION!", origin.getLatitude() + " :  " + origin.getLongitude());
        Log.d("TARGET LOCATION!", locationModel.toString());
/**
 * distanceTo(Location) - compares between to Location variable.
 */
        //Create Location object for user's location.
        Location currentLocation = new Location(" ");
        currentLocation.setLatitude(origin.getLatitude());
        currentLocation.setLongitude(origin.getLongitude());

        //Create Location object for destination location, locationalModel.
        Location destination = new Location(" ");
        destination.setLatitude(locationModel.getLatitude());
        destination.setLongitude(locationModel.getLongitude());

        // compare and return the distance in meters.
        return currentLocation.distanceTo(destination);
    }

    /**
     *Gets the current location of the user from the device GPS.
     */
    public void getCurrentLocation() {

        //test if Location access is granted.
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 0, this);
            if (locationManager != null) {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            origin = new LocationModel(location.getLatitude(), location.getLongitude());
        }else{
            Toast.makeText(context, "Permission Denied!", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Change the coordinates for user's most recent location.
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        origin.setLatitude(location.getLatitude());
        origin.setLongitude(location.getLongitude());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {    }

    @Override
    public void onProviderEnabled(String s) {    }

    @Override
    public void onProviderDisabled(String s) {    }
}
