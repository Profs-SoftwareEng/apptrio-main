package proffs.framework;

/**
 * Created by srd14wau on 25/10/2016.
 */

public abstract class LocationPoint {
    private String name;
    private LocationModel locationModel;

    public LocationModel getLocationModel() {
        return locationModel;
    }

    public void setLocationModel(LocationModel locationModel) {
        this.locationModel = locationModel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //custom methods to be added
}
