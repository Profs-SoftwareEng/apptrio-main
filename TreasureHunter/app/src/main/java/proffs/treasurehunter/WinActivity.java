package proffs.treasurehunter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class WinActivity extends AppCompatActivity {

    TextView finalScore;
    ImageView balloons;
    Button playAgain;

    final Database db = new Database(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Code to display layout
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_win);

        //Set the global current activity variable to Win.
        ((TreasureHunter)getApplicationContext()).setCurrentActivity("Win");

        TreasureHunter th = new TreasureHunter();

        finalScore = (TextView)findViewById(R.id.finalScoreValue);

        finalScore.setText(String.valueOf(th.getTotalScore()));

        playAgain = (Button) findViewById(R.id.playAgain);

        //Add an onclick listener for the button
        playAgain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //On click load home activity
                playAgain();
            }
        });
    }

    //Method for adding a new entry into the database so that the user
    //can play again
    public void playAgain() {
        //Add a new entry into the database.
        db.updateName("complete");

        //Go back to the home page
        Intent loadHomeActivity = new Intent(WinActivity.this, HomeActivity.class);
        startActivity(loadHomeActivity);

    }
}
