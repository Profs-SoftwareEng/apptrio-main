package proffs.treasurehunter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class RulesActivity extends AppCompatActivity {

    TextView finalScore;
    Button homeButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Code to display layout
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules);

        homeButton = (Button)findViewById(R.id.homeButton);

        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loadRulesActivity = new Intent(RulesActivity.this, HomeActivity.class);
                startActivity(loadRulesActivity);
            }
        });
    }

}
