package proffs.treasurehunter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import proffs.framework.LocationModel;
import proffs.framework.Server;
import proffs.framework.LocationPointController;

/**
 * Created by james on 05/01/17.
 */

public class ServerHandler extends Server {
    String TAG = "SERVER";
    Context context;
    Activity activity;

    //set host
    public ServerHandler(Context context, Activity activity){
        super("www.X.4irc.com");
        this.context = context;
        this.activity = activity;
    }


    @Override
    public void processResult(String endpoint, String result) {
        switch(endpoint){
            case "app-trio/get_treasurepoint.php":
                processGetTreasurepoint(result);
                break;
        }
        Log.d(TAG, endpoint + ": " + result);
    }

    @Override
    public void processImageResult(Bitmap result) {
        TreasurePoint tp = ((TreasureHunter)context.getApplicationContext()).getCurrentTreasurePoint();
        tp.setImage(result);
        ((TreasureHunter)context.getApplicationContext()).setCurrentTreasurePoint(tp);
    }

    //ENDPOINT===========================================================

    public void getTest() {
        String params = "a=3";
        hitServer("app-trio/test.php", params);
        Log.d("SERVER", params);
    }

    public void getTreasurePoint(String name) {
        String params = "tr_name=" + name;
        hitServer("app-trio/get_treasurepoint.php", params);
        Log.d("SERVER", params);
    }

    //Process
    public void processGetTreasurepoint(String result){
        Log.d("processGetTreasurepoint", "|"+result+"|");
        TreasurePoint tp = null;

        //If result is complete, take the user to the win page
        if((result.replace("\n", "")).equals("complete")){
            ((TravelingActivity)activity).goToWin();
        }
        else{
        //Parse the JSON data and assign to variables.
        try {
            JSONObject resultData = new JSONObject(result);

            String name = resultData.getString("name");
            String description = resultData.getString("description");
            int value = resultData.getInt("value");
            double latitude = resultData.getDouble("latitude");
            double longitude = resultData.getDouble("longitude");
            tp = new TreasurePoint(name, description, value, latitude, longitude, resultData.getString("image_path"));

            Log.d("tp name: ", name);

            //Set the global current TreasurePoint variable
            ((TreasureHunter)context.getApplicationContext()).setCurrentTreasurePoint(tp);

            //Set the global current Current Reward variable
            ((TreasureHunter)context.getApplicationContext()).setCurrentReward(tp.getValue());

            double targetLat = ((TreasureHunter)context.getApplicationContext()).getCurrentTreasurePoint().getLocationModel().getLatitude();
            double targetLong = ((TreasureHunter)context.getApplicationContext()).getCurrentTreasurePoint().getLocationModel().getLongitude();

            float distBetween = Math.round(new LocationPointController(context).getDistance(new LocationModel(targetLat, targetLong)));
            ((TreasureHunter)context.getApplicationContext()).setDistanceToTarget(distBetween);
            Log.d("sev",distBetween + "--------------------------");
            //Log.d("ServerHandler", "===================>> "+targetLat + "/ " + targetLong + "   " + distBetween);

            Log.d("serverhandler", ((TreasureHunter)context.getApplicationContext()).getCurrentTreasurePoint().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        //Assign variables to the widgets in the traveling activity.
        TextView targetName = (TextView) activity.findViewById(R.id.TargetValue);
        TextView targetValue = (TextView) activity.findViewById(R.id.currentPtsValue);
        TextView totalScore = (TextView) activity.findViewById(R.id.totalScoreValue);
        TextView distance = (TextView) activity.findViewById(R.id.Value);

        //Assign variables to the widgets in the found activity.
        TextView foundDescription = (TextView) activity.findViewById(R.id.locationFoundDesc);
        TextView foundTotalScore = (TextView) activity.findViewById(R.id.totalScoreValue);
        TextView locationFound = (TextView) activity.findViewById(R.id.locationFound);


        //Testing
        Log.d("serv", ((TreasureHunter)context.getApplicationContext()).getCurrentActivity());


        //Update the text in the TextViews in the traveling activity to display the
        //correct information
        if((((TreasureHunter) context.getApplicationContext()).getCurrentActivity() == "Traveling"))
        {
            if (tp != null) {
                //Set target name.
                targetName.setText(tp.getName());

                //Set current reward.
                targetValue.setText(String.valueOf(((TreasureHunter)context.getApplicationContext()).getCurrentReward()));

                //Set total score.
                totalScore.setText(String.valueOf(((TreasureHunter)context.getApplicationContext()).getTotalScore()));

                //Set distance to target
                distance.setText(String.valueOf(((TreasureHunter)context.getApplicationContext()).getDistanceToTarget()).replace(".0", ""));
            }
        }

        //Update the text in the TextViews in the found activity to display the
        //correct information
        if((((TreasureHunter) context.getApplicationContext()).getCurrentActivity() == "Found")) {
            if (tp != null) {
                //Set target description.
                foundDescription.setText(tp.getDescription());
                //Set total score.
                foundTotalScore.setText(String.valueOf(((TreasureHunter)context.getApplicationContext()).getTotalScore()));
                //Set target name as title.
                locationFound.setText(((TreasureHunter) context.getApplicationContext())
                        .getCurrentTreasurePoint().getName());
                locationFound.append(" Found!");
            }
        }

        Log.d("IMAGE PATH", tp.getImage_path());
        imageHitServer(tp.getImage_path());
        }
    }

}
