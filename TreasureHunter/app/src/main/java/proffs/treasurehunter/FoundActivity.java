package proffs.treasurehunter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class FoundActivity extends AppCompatActivity {

    //Variables for activity widgets
    TextView locFound;
    TextView totalScore;
    TextView targetDesc;
    ImageView targetImage;
    Button foundButton;

    //Variable for server handler
    ServerHandler serv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Code to display layout
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_found);

        //Set the global current activity variable to Found
        ((TreasureHunter)getApplicationContext()).setCurrentActivity("Found");

        //Get the current treasure point stored in a global variable
        TreasurePoint tp = (((TreasureHunter) getApplicationContext()).getCurrentTreasurePoint());

        //Instantiate TreasureHunter and ServerHandler Objects.
        TreasureHunter th = new TreasureHunter();
        serv = new ServerHandler(getApplicationContext(), this);

        //Assign variables to corresponding widgets in the activity
        locFound = (TextView)findViewById(R.id.locationFound);
        totalScore= (TextView)findViewById(R.id.totalScoreValue);
        targetDesc = (TextView)findViewById(R.id.locationFoundDesc);
        targetImage = (ImageView)findViewById(R.id.locationFoundImage);

        //If current treasure point is not null, update text views with relevant information
        if (tp != null) {
            //Set the target's description
            targetDesc.setText(tp.getDescription());

            //Set the total score
            totalScore.setText(String.valueOf(th.getTotalScore()));

            //Set the name of the found location
            locFound.setText(((TreasureHunter) getApplicationContext())
                    .getCurrentTreasurePoint().getName());
            locFound.append(" Found!");

            //Set the location's image
            targetImage.setImageBitmap(tp.getImage());
        }


        //Code for navigation and buttons

        //Assign button to widget in the activity
        foundButton = (Button) findViewById(R.id.continueButton);

        //Add an onclick listener for the button
        foundButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //On click load new activity
                Intent loadNewTravelActivity = new Intent(FoundActivity.this, TravelingActivity.class);
                Intent loadNewWinActivity = new Intent(FoundActivity.this, WinActivity.class);


//                if ((((TreasureHunter) getApplicationContext()).getCurrentTreasurePoint()).getName().equals(
//                        "Sainsbury Centre for Visual Arts")) {
//                    startActivity(loadNewWinActivity);
//                }
//                else {
                    startActivity(loadNewTravelActivity);
//                }
            }
        });

    }
}
