package proffs.treasurehunter;

import android.app.Application;
import android.location.Location;

/**
 * Created by Micky on 10/01/2017.
 */

public class TreasureHunter extends Application{

    //Global variables for entire game
    private TreasurePoint currentTreasurePoint = null;
    private static int totalScore = 0;
    private static int currentReward;
    private String currentActivity;
    private float distanceToTarget = 0;

    public float getDistanceToTarget() {
        return distanceToTarget;
    }

    public void setDistanceToTarget(float distanceToTarget) {
        this.distanceToTarget = distanceToTarget;
    }
    //Getters and setters for global variables.

    //CurrentActivity getters and setters
    public String getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(String currentActivity) {
        this.currentActivity = currentActivity;
    }


    //Current TreasurePoint getters and setters
    public TreasurePoint getCurrentTreasurePoint() {
        return currentTreasurePoint;
    }

    public void setCurrentTreasurePoint(TreasurePoint currentTreasurePoint) {
        this.currentTreasurePoint = currentTreasurePoint;
    }


    //Current Reward getters and setters
    public int getCurrentReward() {
        return currentReward;
    }

    public void setCurrentReward(int currentReward) {
        this.currentReward = currentReward;
    }


    //Total Score getters and setters
    public int getTotalScore() {
        return totalScore;
    }

    //Add current reward to user's total score
    public void addToCurrentScore(int add) {
        totalScore += add;
    }

    //Set the total score global variable to the user's overall score
    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    //If the user refreshes and they are not in range of their target destination
    //lower the current reward by 10.
    public void lowerReward() {
        //If the current reward is 0 do not lower the reward.
        if (currentReward == 0) {
            //Do nothing
            currentReward -= 0;
        }
        //Otherwise lower the current reward by 10.
        else {
            currentReward -= 10;
        }
    }


}
