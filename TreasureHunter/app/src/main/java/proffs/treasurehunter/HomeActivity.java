package proffs.treasurehunter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import proffs.framework.LocationPointController;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class HomeActivity extends AppCompatActivity {

    private static final int MY_PERMISSION_REQUEST_LOCATION = 1;

    //Variables for activity widgets
    ImageView clearButton;
    ImageView goButton;
    Button rulesButton;
    //Database instantiation
    final Database db = new Database(this);


    LocationPointController locationPointController;
    ImageView header;

//    Audio audio;
//    public static Music theme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Code to display layout
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //theme.stop();

        header = (ImageView) findViewById(R.id.imageView);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                testCurrentLoc();
            }
        });

        final TreasureHunter th = new TreasureHunter();


        //If the current database is empty, add new entry of "new"
        if (db.getCurrent() == null) {
            db.addEntry("new");
        }

        //Set the current total score to the total score from the database
        th.setTotalScore(db.getCurrent().getValue());

        TreasurePoint tp = new TreasurePoint(db.getCurrent().getName());
        ((TreasureHunter) getApplicationContext()).setCurrentTreasurePoint(tp);

        //Set the global current activity variable to Home.
        ((TreasureHunter)getApplicationContext()).setCurrentActivity("Home");

        //Assign variables to corresponding widgets in the activity
        clearButton = (ImageView) findViewById(R.id.clearButton);
        goButton = (ImageView) findViewById(R.id.goButton);
        rulesButton = (Button) findViewById(R.id.howToPlay);

        //Add an onclick listener for the go button
        goButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!checkLocationPermission()) {
                    requestPermissionLocation();
                } else {
                    Intent loadNewActivity = new Intent(HomeActivity.this, TravelingActivity.class);
                    startActivity(loadNewActivity);
                }
            }
        });


        rulesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loadRulesActivity = new Intent(HomeActivity.this, RulesActivity.class);
                startActivity(loadRulesActivity);
            }
        });

        //Add an onclick listener for the clear button
        clearButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Clear the current score saved in the database
                db.deleteCurrentScore();

                //Display message on screen to confirm database has been wiped.
                Context context = getApplicationContext();
                CharSequence text = "Progress cleared!";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                //Set the Treasure Point to the first Treasure Point
                db.addEntry(("new"));


                //Set the current total score to the total score from the database
                th.setTotalScore(db.getCurrent().getValue());

                //Set the current Treasure Point to the current treasure point
                //in the database
                TreasurePoint tp = new TreasurePoint(db.getCurrent().getName());
                ((TreasureHunter) getApplicationContext()).setCurrentTreasurePoint(tp);
            }
        });
    }
//    public Audio getAudio() {
//        return audio;
//    }

    public void openRules() {
        Intent loadRulesActivity = new Intent(HomeActivity.this, RulesActivity.class);
        startActivity(loadRulesActivity);
    }

    public void testCurrentLoc(){
        locationPointController = new LocationPointController(getApplicationContext());

//        Toast.makeText(getApplicationContext(), locationPointController.getPoints().getLatitude() + "," +
//                locationPointController.getPoints().getLongitude(), Toast.LENGTH_LONG).show();
    }
    private boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            return true;
        return false;
    }

    private void requestPermissionLocation() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, MY_PERMISSION_REQUEST_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case MY_PERMISSION_REQUEST_LOCATION:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(getApplicationContext(), "Permission Granted!", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }
}