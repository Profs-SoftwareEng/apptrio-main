package proffs.treasurehunter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "treasureHunterDB";
    private static final String TABLE_SCORES = "scores";
    private static final String KEY_TP_NAME = "tp_name";
    private static final String KEY_POINTS = "past_points";

    public Database(Context context){
        super (context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db){
        String CREATE_SCORES_TABLE = "CREATE TABLE " + TABLE_SCORES + "(" +KEY_TP_NAME + " TEXT," + KEY_POINTS
                + " INTEGER" + ")";
        db.execSQL(CREATE_SCORES_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCORES);
        onCreate(db);
    }

    //adding new treasure point
    public void addEntry(String tpName){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TP_NAME, tpName);
        values.put(KEY_POINTS, 0);
        //inserting row
        db.insert(TABLE_SCORES, null, values);
        db.close(); //closes database connection
    }
    //get single entry
    public TreasurePoint getCurrent(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_SCORES, new String[] {}, KEY_TP_NAME + " != ?", new String[] {"complete"},"", "", "");
        if (cursor != null && cursor.moveToFirst()){
            TreasurePoint entry = new TreasurePoint(cursor.getString(0), cursor.getInt(1));
            return entry;
        }
        return null;
    }
    //get all scores
    public List<TreasurePoint> getAllEntrys(){
        List<TreasurePoint> entryList = new ArrayList<TreasurePoint>();
        // select all query
        String selectQuery = "SELECT * FROM " + TABLE_SCORES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        //looping through all rows and adding to list
        if (cursor.moveToFirst()){
            do {
                TreasurePoint entry = new TreasurePoint(cursor.getString(0),cursor.getInt(1));
                entryList.add(entry);
            } while (cursor.moveToNext());
        }
        return entryList;
    }
    //updating a score
    public int updateScore(int score){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_POINTS, score);
        //updating row
        return db.update(TABLE_SCORES, values, KEY_TP_NAME + " != ?", new String[]{"complete"});
    }
    //updating the name
    public int updateName(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TP_NAME, name);
        //updating row
        return db.update(TABLE_SCORES, values, KEY_TP_NAME + " != ?", new String[]{"complete"});
    }
    // deleting a room entry from the history list
    public void deleteCurrentScore(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SCORES, KEY_TP_NAME + " != ?", new String[]{"complete"});
        db.close();
    }
    // delete all records
    public void deleteAll(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SCORES, null, null);
        db.close();
    }
}