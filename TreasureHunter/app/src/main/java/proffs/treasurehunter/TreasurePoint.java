package proffs.treasurehunter;

import android.graphics.Bitmap;

import proffs.framework.LocationModel;
import proffs.framework.LocationPoint;

/**
 * Created by Micky on 10/01/2017.
 */

public class TreasurePoint extends LocationPoint {



    //TreasurePoint variables
    private int value;
    private String description;
    private Bitmap image;
    private String imagePath;

    //TreasurePoint Constructor with all variables
    public TreasurePoint (String name, String description, int value, double latitude, double longitude, String imagePath) {
        super.setLocationModel(new LocationModel(latitude, longitude));
        super.setName(name);
        this.value = value;
        this.description = description;
        this.imagePath = imagePath;
    }

    //Constrcutor for TreasurePoint with just a name variable
    public TreasurePoint (String name) {
        super.setName(name);
    }
    public TreasurePoint (String name, int value) {
        super.setName(name);
        this.value = value;
    }
    //Getter and Setter methods

    //Get and Set methods for value
    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }



    //Get and Set methods for description.
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    //Get and Set methods for image path.
    public String getImage_path() {
        return imagePath;
    }

    public void setImage_path(String imagePath) {
        this.imagePath = imagePath;
    }



    //Get and Set methods for description.
    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }



    //toString method for a TreasurePoint object.
    public String toString() {
        return super.getName();
    }
}
