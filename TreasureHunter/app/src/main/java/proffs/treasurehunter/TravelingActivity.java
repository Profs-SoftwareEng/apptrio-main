package proffs.treasurehunter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import proffs.framework.Audio;
import proffs.framework.LocationModel;

import proffs.framework.Music;
import proffs.framework.implementation.AndroidAudio;

import proffs.framework.LocationPointController;

public class TravelingActivity extends AppCompatActivity {

    //Variables for activity widgets
    Button refreshButton;
    TextView totalScoreValue;
    TextView currentReward;
    TextView targetText;
    TextView distanceValue;
    ImageView muteButton;

    //Variable for server handler
    ServerHandler serv;

    //Database instantiation
    final Database db = new Database(this);

    public void goToWin () {
        //go to win
        Intent win = new Intent(TravelingActivity.this, WinActivity.class);
        startActivity(win);
    }

    Audio audio;

    public static Music theme;
    boolean playing = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Code to display layout
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traveling);

        audio = new AndroidAudio(this);

        theme = getAudio().createMusic("menutheme.mp3");
        theme.setLooping(true);
        theme.setVolume(0.85f);


        //Set the global current activity variable to traveling.
        ((TreasureHunter)getApplicationContext()).setCurrentActivity("Traveling");

        final List<TreasurePoint> entryList = db.getAllEntrys();
        for (TreasurePoint i: entryList){
            Log.d("Name", i.getName());
            Log.d("Score: ", String.valueOf(i.getValue()));
        }

        Log.d("current tp name: ", (((TreasureHunter)getApplicationContext()).getCurrentTreasurePoint().getName()));
        Log.d("Database: ", "END OF DATABASE TESTING----------------------------------------");

        //Code for navigation and buttons

        //Assign refresh button to widget in the activity
        refreshButton = (Button) findViewById(R.id.RefreshLocation);

        //Assign mute button to widget in the activity
        muteButton = (ImageView)findViewById(R.id.muteButton);

        //Add an onclick listener for the refresh button
        muteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //On click call the updateLocation method
                if(playing) {
                    theme.stop();
                    Log.d("click", "STOP");
                    playing = false;
                }else {
//                    theme = getAudio().createMusic("menutheme.mp3");
//                    theme.setLooping(true);
//                    theme.setVolume(0.85f);
                    theme.play();
                    playing = true;
                }
            }
        });

        //Add an onclick listener for the refresh button
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //On click call the updateLocation method
                updateLocation();
            }
        });

        totalScoreValue = (TextView) findViewById(R.id.currentPtsValue);
        targetText = (TextView) findViewById(R.id.TargetValue);
        distanceValue = (TextView) findViewById(R.id.Value);

        //Instantiate serverhandler object
        serv = new ServerHandler(getApplicationContext(), this);

        //If there is no current treasure point then return the first treasure point
        if ((((TreasureHunter) getApplicationContext()).getCurrentTreasurePoint() == null)){
            serv.getTreasurePoint("new");
            Log.d("---------------", "new");
        }
        //otherwise return the next treasure point
        else {
            serv.getTreasurePoint(((TreasureHunter) getApplicationContext()).getCurrentTreasurePoint().getName());
        }
    }
    public Audio getAudio() {
        return audio;
    }

    //Method for seeing if the user us within range of the target location
    public void updateLocation() {
        TreasurePoint treasurePoint = ((TreasureHunter)getApplicationContext()).getCurrentTreasurePoint();
        if (treasurePoint == null || treasurePoint.getLocationModel() == null){
            return;
        }

        //Instantiation of TreasureHunter object
        TreasureHunter th = new TreasureHunter();

        double distance = getDistance(treasurePoint.getLocationModel());
        Log.d("traveling DIS", distance + "   <<<<<<<<<<<<<<<<<<<<<<<<<<<<,");
        distanceValue.setText(Double.toString(Math.floor(distance)).replace(".0", ""));

        //If the user is within range of the target location
        if (((TreasureHunter)getApplicationContext()).getDistanceToTarget() < 30) {

            //Increment their total score by the current reward
            th.addToCurrentScore(th.getCurrentReward());
            db.updateScore(db.getCurrent().getValue() + th.getCurrentReward());

            //Set the current treasure point to the target location
            TreasurePoint current = ((TreasureHunter)getApplicationContext()).getCurrentTreasurePoint();

            ((TreasureHunter)getApplicationContext()).setCurrentTreasurePoint(current);
            db.updateName(current.getName());

            //Navigate to the found activity
            Intent loadNewActivity = new Intent(TravelingActivity.this, FoundActivity.class);
            startActivity(loadNewActivity);

        }

        //If the user is not within range of the target location
        else {
            //Call the lower reward method
            th.lowerReward();

            //Update the current reward on the screen with it's new lowered value
            currentReward = (TextView) findViewById(R.id.currentPtsValue);
            currentReward.setText(String.valueOf(th.getCurrentReward()));

        }
    }

    /**
     *  Get the distance between origin and given destination.
     * @param locationModel destination LocationModel
     * @return
     */
    public float getDistance(LocationModel locationModel) {
        LocationPointController locationPointController = new LocationPointController(getApplicationContext());
        locationPointController.getDistance(locationModel);

        return locationPointController.getDistance(locationModel);
    }
}
